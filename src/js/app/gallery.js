var gallery = (function() {
  var me = {};

  me.init = function() {
    // main slider
    $('.js-gallery-slider').slick({
      arrows: false,
      // accessibility: false,
      onAfterChange: onAfterChange,
      onInit: onInit
    });

    // nav slider
    $('.js-gallery-navs').slick({
      arrows: false,
      infinite: false,
      dots: true,
      // draggable: false,
      accessibility: false
    });

    $(document).on('tap', '.js-gallery-item', onNavsItemClick);
    $(document).on('tap', '.js-gallery-left', onLeftArrowClick);
    $(document).on('tap', '.js-gallery-right', onRightArrowClick);
  };

  function onAfterChange(e) {
    var $slider = e.$slider,
        $navs = $slider.closest('.js-gallery').find('.js-gallery-navs'),
        // currentSlide = $slider.slickCurrentSlide(),
        currentSlide = e.currentSlide,
        navItemsPerSlide = $navs.data('items').length ? $navs.data('items') : 8,
        navsCurrentSlide = $navs.slickCurrentSlide(),
        navsCalculatedSlide = Math.floor(currentSlide / navItemsPerSlide);


    $navs.find('.js-gallery-item.active').removeClass('active');
    $navs.find('.js-gallery-item[data-slide='+currentSlide+']').addClass('active');

    $slider.closest('.js-gallery').find('.js-gallery-current-slide').text(currentSlide+1);

    if (navsCalculatedSlide !== navsCurrentSlide) {
      $navs.slickGoTo(navsCalculatedSlide);
    }
  }

  function onInit(e) {
    var $slider = e.$slider;
    // console.log(e);
    $slider.closest('.js-gallery').find('.js-gallery-total').text(e.slideCount);
  }

  function onNavsItemClick(e) {
    e.preventDefault();
    var $self = $(this);
    if (!$self.hasClass('active')) {
      $self.closest('.js-gallery').find('.js-gallery-slider').slickGoTo($self.data("slide"));
    }
  }

  function onLeftArrowClick(e) {
    e.preventDefault();
    $(this).closest('.js-gallery').find('.js-gallery-slider').slickPrev();
  }

  function onRightArrowClick(e) {
    e.preventDefault();
    $(this).closest('.js-gallery').find('.js-gallery-slider').slickNext();
  }


  return me;
}());