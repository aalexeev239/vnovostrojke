var leaveComment = (function() {
  var me = {};

  me.init = function() {
    $(document).on('focusin','.js-leave-comment.closed',onFocusIn);
    $(document).on('submit','.js-leave-comment',onSubmit);
    $(document).on('focusout keypress','.js-validate',onFocusOut);
  };

  function onFocusIn() {
    $(this).removeClass('closed');

  }

  function onSubmit(e) {
    e.preventDefault();
    var $self = $(this);
    // console.log('self');
    // console.log($self);

    if (validate($self)) {
      // do smth
      console.log('submited');
    }
  }

  function validate($form) {
    var $inputs = $form.find('.js-validate');
    $inputs.each(function(index, el) {
      if (! $(el).val()) {
        $(el).addClass('error');
      } else {
        $(el).removeClass('error');
      }
    });

    if ($inputs.filter('.error').length) {
      $form.find('.js-submit').addClass('disabled');
      return false;
    } else {
      $form.find('.js-submit').removeClass('disabled');
      return true;
    }
  }

  function onFocusOut() {
    var $self = $(this),
        $form = $self.closest('.js-leave-comment'),
        $inputs = $form.find('.js-validate');

    if ($self.hasClass('error') && $self.val()) {
      $self.removeClass('error');
    }

    if ($form.find('.js-submit').hasClass('disabled') && (! $inputs.filter('.error').length)) {
      var isValid = true;

      $inputs.each(function(index, el) {
        if (! $(el).val()) {
          isValid = false;
        }
      });

      if (isValid) {
        $form.find('.js-submit').removeClass('disabled');
      }
    }
  }

  return me;
}());