var showMore = (function() {
  var me = {};

  me.init = function() {
    $(document).on('tap', '.js-show-more', onMasterClick);
  };

  // Shows element by id written in 'href' by default
  // If data-target is specified and selector it contains doesn't return empty values
  // elements with that selector will be shown
  function onMasterClick(e) {
    var $self = $(this),
        $target = $($self.data('target')).length ? $($self.data('target')) : $($self.attr('href'));

    e.preventDefault();
    if ($target.length) {
      $target.filter(':hidden').slideDown(400);
    }
    $self.hide();
  }

  return me;
}());