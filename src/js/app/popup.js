var popup = (function() {
  var me = {};

  me.init = function() {
    $(document).on('tap', 'html', closePopup);
    $(document).on('tap mousein', '.js-popup', onPopupTapped);
    $(document).on('tap mouseout', '.js-popup', onPopupTapped);
  };

  function onPopupTapped(e) {
    var $self = $(this);
    e.preventDefault();
    e.stopPropagation();
    // $self.toggleClass('hover');
    if (! $self.hasClass('hover')) {
      $('.js-popup.hover').removeClass('hover');
      $self.addClass('hover');
    } else {
      $self.removeClass('hover');
    }
  }

  function closePopup() {
    $('.js-popup.hover').removeClass('hover');
  }

  return me;
}());