var slider = (function() {
  var me = {};

  me.init = function() {
    $('.js-slider').slick({
      arrows: false,
      slide: "img",
      onAfterChange: onAfterChange,
      onInit: onInit
    });

    $(document).on('tap', '.js-slider-left', onLeftArrowClick);
    $(document).on('tap', '.js-slider-right', onRightArrowClick);
  };

  function onAfterChange(e) {
    e.$slider
      .find('.js-slider-current-slide')
      .text(e.currentSlide+1);
  }

  function onInit(e) {
    e.$slider
      .find('.js-slider-total')
      .text(e.slideCount);
  }

  function onLeftArrowClick(e) {
    e.preventDefault();
    $(this)
      .closest('.js-slider')
      .slickPrev();
  }

  function onRightArrowClick(e) {
    e.preventDefault();
    $(this)
      .closest('.js-slider')
      .slickNext();
  }



  return me;
}());