var toggleContent = (function() {
  var me = {};

  me.init = function() {
    $(document).on('tap', '.js-toggle-content', onMasterClick);
  };

  // Shows element by id written in 'href' by default
  // If data-target is specified and selector it contains doesn't return empty values
  // elements with that selector will be shown
  function onMasterClick(e) {
    var $self = $(this),
        $target = $($self.data('target')).length ? $($self.data('target')) : $($self.attr('href'));
    e.preventDefault();
    if ($target.length) {
      $target.slideToggle(400)
    }
    $self.toggleClass('active');
  }

  return me;
}());