var toggleLikes = (function() {
  var me = {};

  me.init = function() {
    $('.js-counter').each(function(){
      updateCounter($(this),'init',0);
    });
    $(document).on('tap','.js-like',onLikeClick);
    $(document).on('tap','.js-dislike',onDislikeClick);
  };

  function onLikeClick(e) {
    e.preventDefault();
    var $self = $(this),
        $counter = $(this).closest('.js-toggle-likes').find('.js-counter');
    if ($self.hasClass('active')) {
      $self.removeClass('active');
      updateCounter($counter,'minus',1);
    } else {
      $self.addClass('active')
      var $dislike = $self.closest('.js-toggle-likes').find('.js-dislike');
      if ($dislike.hasClass('active')) {
        $dislike.removeClass('active');
        updateCounter($counter,'plus',2);
      } else {
        updateCounter($counter,'plus',1);
      }

    }
  }

  function onDislikeClick(e) {
    e.preventDefault();
    var $self = $(this),
        $counter = $(this).closest('.js-toggle-likes').find('.js-counter');
    if ($self.hasClass('active')) {
      $self.removeClass('active');
      updateCounter($counter,'plus',1);
    } else {
      $self.addClass('active');
      var $like = $self.closest('.js-toggle-likes').find('.js-like');
      if ($like.hasClass('active')) {
        $like.removeClass('active');
        updateCounter($counter,'minus',2);
      } else {
        updateCounter($counter,'minus',1);
      }
    }
  }

  function updateCounter($counter, action, value) {
    var currentValue = + $counter.text();
    switch (action) {
      case 'plus':
        currentValue += value;
        break;
      case 'minus':
        currentValue -= value;
        console.log(currentValue);
        break;
      default:
        break;
    }
    if (currentValue > 0) {
      $counter.addClass('positive').removeClass('negative');
    } else if (currentValue < 0) {
      $counter.addClass('negative').removeClass('positive');
    } else {
      $counter.removeClass('positive negative');
    }
    $counter.text(currentValue);
    return false;
  }

  return me;
}());