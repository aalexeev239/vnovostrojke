var articleSlider = (function() {
  var me = {};

  me.init = function() {
    $('.js-article-slider').slick({
      slide: "a",
      slidesToShow: 4
    });
  };

  return me;
}());