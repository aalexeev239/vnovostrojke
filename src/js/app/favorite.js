var favorite = (function() {
  var me = {};

  me.init = function() {
    $(document).on('tap', '.js-fav-btn', onFavClick);
  };

  function onFavClick(e) {
    e.preventDefault();
    $(this).toggleClass('active');
  }

  return me;
}());