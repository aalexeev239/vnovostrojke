var fixedNav = (function() {
  var me = {},
      lastscroll = 0,
      $fixedNav = $('#js-fixed-nav'),
      $fixedNavReturn = $('#js-fixed-nav-return'),
      $originalNav = $('#js-fixed-nav-origin');


  me.init = function() {
    $(window).on('scroll', onScroll);
  };

  function onScroll() {
    var currentScroll = $(this).scrollTop(),
        originalNavOffset = $originalNav.length ? $originalNav.offset().top : 0;


    if (currentScroll < originalNavOffset) {
        $fixedNav.slideUp(200);
        if ($fixedNavReturn.length) {
          $fixedNavReturn.addClass('hidden');
        }

    } else {
      $fixedNav.slideDown(200);
      if ($fixedNavReturn.length) {
        if (currentScroll < lastscroll) {
          $fixedNavReturn.removeClass('hidden');
        } else {
          $fixedNavReturn.addClass('hidden');
        }
      }
    }

    lastscroll = currentScroll;
  }

  return me;
}());