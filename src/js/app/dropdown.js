var dropdown = (function() {
  var me = {};

  me.init = function() {
    $(document).on('tap', '.js-dropdown-control', onControlClick);
    $(document).on('keydown', dropdownCloseKeyDown);
    $(document).on('tap', '.js-dropdown-close', onCloseBtnClick);
    // clicking outside
    $(document).on('tap', 'html', closeDropdown);
    $(document).on('tap', '.js-dropdown-body ', preventCloseDropdown);
  };

  function onControlClick(e) {
    e.preventDefault();
    e.stopPropagation();
    $(this).closest('.js-dropdown').toggleClass('active');
  }

  function dropdownCloseKeyDown(e) {
    if (e.which == 27) {
      $('.js-dropdown.active').removeClass('active');
    }
  }

  function onCloseBtnClick(e) {
    e.preventDefault();
    $(this).closest('.js-dropdown').removeClass('active');
  }

  function closeDropdown() {
    $('.js-dropdown.active').removeClass('active');
  }

  function preventCloseDropdown(e) {
    e.stopPropagation();
  }

  return me;
}());