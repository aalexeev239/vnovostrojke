// SelectBox https://github.com/marcj/jquery-selectBox

var search = (function() {
  var me = {};


  // js-search
  // js-search-slider
  // js-search-slider-slides
  // js-search-tab
  me.init = function() {

    // Refactoring to new search
    // [AA] 2015/05/10
    /*==========  selects:  ==========*/
    // $('select.search__select')
    //   .selectBox({
    //     mobile: true,
    //     bottomPositionCorrelation: 30
    //   });
    // $('select.search__select').selectBox().change(setShortValue);
    // $('select.search__select').each(setShortValue);
    // checkAdditionalInfo();
    //   changePriceValue();
    //   setPriceToAdvanced();
    /*==========  submit  ==========*/
    //$(document).on('submit', '.js-search', onSearchSubmit);
    // $('.js-search').on('search.success', onSearchSuccess)

    $('.js-search-slider').filter(':visible').each(function(index, el) {
      me.initSlider($(this));
    });

    $(document).on('click', '.js-search-letter', onLetterClicked);

    var $tabs = $('.js-tabs-search');


    // init and reinit sliders on tab change
    $tabs.on('tabs.afterChange', function(event) {

      var $t = $(this);
      var $active = $t.find('.js-tabsItem.active').filter(function(){
        return $(this).closest('.js-tabs').is($t);
      });
      var $sliders = $active.find('.js-search-slider');


      $sliders.each(function(index, el) {
        me.initSlider($(this));
      });
    });


    $(window).on('resize', function() {
      $('.js-search-slider-slides').filter(':not(:visible)').each(function(index, el) {

        if ($(el).hasClass('slick-slider')) $(el).slick('unslick');

        $(el).closest('.js-search-slider').removeData('inited');
      });
    });

  };







  me.initSlider = function($sl, $slides) {

    !$sl.data('inited') && $sl.addClass('loading');

    var imgLoad = imagesLoaded($sl);

    if ($slides) {
      imgLoad.off('done',onSliderImagesLoaded);
      $sl.addClass('loading').data('inited', false);
      $sl.find('.js-search-slider-slides.slick-slider').slick('unslick');
      $sl.html($slides);
    }
    imgLoad.on('done',onSliderImagesLoaded);
  }




  function onSliderImagesLoaded (instance) {
    // cont = js-search-cliser
    var $cont = $(instance.elements[0]),
        $slider = $cont.find('.js-search-slider-slides');

    if (!$cont.data('inited')) {
      $slider.slick({
        slidesToShow: 4,
        adaptiveHeight: true,
        rows: 2
      });
    }

    $cont.data('inited', true).removeClass('loading');
  }



  function onLetterClicked(ev) {
    ev.preventDefault();
    var $letter = $(this),
        $cont = $letter.closest('.js-search-tab');

    $cont.find('.js-search-letter.active').removeClass('active');
    $letter.addClass('active')

    $.ajax({
      // url: encodeURIComponent($letter.attr('href'))+'?nocache='+new Date().getTime()
      url: 'get-by-letter-a.json?'+new Date().getTime(),
      type: 'GET',
      dataType: 'json',
    })
    .done(function(data, response, jqXHR) {
      if (!data) return;

      var length = data.length,
          templ = $('#search-slider-template').html(),
          $slider = $letter.closest('.js-search-tab').find('.js-search-slider'),
          markup;

      Mustache.parse(templ);

      markup = Mustache.render(templ, data);

      $slider.length && me.initSlider($slider, $(markup));
    })
    .fail(function() {
      // console.log("error");
    })
    .always(function() {
      // console.log("complete");
    });
  }



  function addRuleForAddSearch($val){
      $('#advanced').val($val);
      setPriceToAdvanced();
  }



  function onSearchSubmit(e){
    e.preventDefault();
    $(this).trigger('search.success');
  }

  // function onSearchSuccess() {
  //   alert('success');
  //   $(this).find('.js-popover').addClass('popover--shown');
  // }

  // Refactoring to new search
  // [AA] 2015/05/10

  // function checkAdditionalInfo(){
  //     if($('#citySelect').val() == "0"){
  //         $('.search__input-group--location').css('display', 'block');
  //     }else{
  //         $('.search__input-group--location').css('display', 'none');
  //     }
  // }

  // function changePriceValue(){
  //     $('#range-min').change(function() {
  //         getPriceValue();
  //     });

  //     $('#range-max').change(function() {
  //         getPriceValue();
  //     });

  //     $('#price').change(function() {
  //         setPriceToAdvanced();
  //     });
  // }

  // function setPriceToAdvanced()
  // {
  //     var $default = $('#price :selected').val();
  //     if($('#price :selected').val() != 'undefined') {
  //         $priceArray = $('#price :selected').val().split('-');
  //         if($priceArray == 'undefined' || $priceArray == ''){
  //             $priceArray = $default;
  //         }

  //         if ($priceArray[0] != 'undefined' && $priceArray[0] != '') {
  //             $('#range-min').val($priceArray[0]);
  //         } else {
  //             $('#range-min').val('');
  //         }
  //         if ($priceArray[1] != 'undefined' && $priceArray[1] != '') {
  //             $('#range-max').val($priceArray[1]);
  //         } else {
  //             $('#range-max').val('');
  //         }
  //     }
  // }

  // function getPriceValue()
  // {
  //     var $default = $('#price :selected').val();
  //     if($('#range-min').val() != '' && $('#range-min').val() != 'undefined'
  //         && $('#range-max').val() != '' && $('#range-max').val() != 'undefined')
  //     {

  //         $('#price :selected').val($('#range-min').val() + '-' + $('#range-max').val());
  //         $('.search__select-wrap--price .selectBox .selectBox-label')
  //             .text($('#range-min').val() + ' - ' + $('#range-max').val() + ' млн.');
  //     }else if($('#range-max').val() != '' && $('#range-max').val() != 'undefined' &&
  //         ($('#range-min').val() == '' || $('#range-min').val() == 'undefined'))
  //     {

  //         $('#price :selected').val('-' + $('#range-max').val());
  //         $('.search__select-wrap--price .selectBox .selectBox-label')
  //             .text('до ' + $('#range-max').val() + ' млн.');
  //     }else if($('#range-min').val() != '' && $('#range-min').val() != 'undefined' &&
  //         ($('#range-max').val() == '' || $('#range-max').val() == 'undefined'))
  //     {

  //         $('#price :selected').val($('#range-min').val() + '-');
  //         $('.search__select-wrap--price .selectBox .selectBox-label')
  //             .text($('#range-min').val() + '+ млн.' );
  //     }else{
  //         $('#price :selected').val($default);
  //     }
  // }

  // function setShortValue() {
  //   // modified '../plugins/jquery.selectBox.js':675 to get change event after every select
  //   // [AA] 2014/12/09
  //     checkAdditionalInfo();
  //   var $self = $(this),
  //     short = $(this).find('option:selected').first().data('short');
  //   if (short) {
  //     $self.selectBox('control').find('.selectBox-label').text(short);
  //   }
  // }

  // function onInputChanged(){
  //   var $self = $(this),
  //       $tabs = $self.closest('.js-tabs'),
  //       output   = '',
  //       $checked = '';
  //   if ($tabs.length) {
  //     // hide other tabs
  //     $tabs.find('.js-tabsItem:not(.active) input').prop('checked', false);
  //     $checked = $tabs.find('.js-tabsItem.active input:not(.js-checkbox-master):checked');
  //   } else {
  //     $checked = $self.closest('.js-dropdown').find('input:not(.js-checkbox-master):checked');
  //   }
  //   if ($checked.length > 1) {
  //     output = '' + $checked.length + ' ';
  //     output += $checked.first().data("item") ? $checked.first().data("item") : 'объектов';
  //   } else if ($checked.length === 1) {
  //     output = $checked.closest('.checkbox').find('.checkbox__label').text();
  //   }

  //   if (output.length) {
  //     $self.closest('.js-dropdown').find('.js-dropdown-control').html(output);
  //   } else {
  //     $self.closest('.js-dropdown').find('.js-dropdown-control').html('<span class="muted">выберите расположение</span>');
  //   }
  // }

  return me;
}());
