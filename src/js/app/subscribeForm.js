var subscribeForm = (function() {
  var me = {};

  me.init = function() {
    $(document).on('submit','.js-subscribe form',onSubmit);
    $('.js-subscribe').on('success', onSuccess);
  };

  function onSubmit(e) {
    e.preventDefault();
    $(this).closest('.js-subscribe').trigger('success');
  }

  function onSuccess() {
    $(this).addClass('success');
  }

  return me;
}());