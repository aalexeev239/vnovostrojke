var modals = (function() {
  var me = {};

  me.init = function() {

    $(document).on('confirm', '.js-modal', onModalConfirmed);

  };

  function onModalConfirmed() {
    var $modal = $(this),
        inst = $.remodal.lookup[$modal.data('remodal')];
    inst.close();
  }

  return me;
}());