var checkboxDependencies = (function() {
  var me = {};

  me.init = function() {
    $(document).on('change', '.js-checkbox', onCheckboxMasterChange);
    $(document).on('change', '.js-checkbox-slave', onCheckboxSlaveChange);
  };


  function onCheckboxMasterChange(){
    var $self = $(this),
        isChecked = $self.is(':checked'),
        slaveValue = $self.data('master');
    if (slaveValue) {
      $('.js-checkbox-slave-'+slaveValue).prop('checked',isChecked);
    }
  }

  function onCheckboxSlaveChange(){
    var $self = $(this);
    if (!$self.is(':checked')) {
      var classList = $self.attr('class').split(/\s+/),
          string = 'js-checkbox-slave-',
          masterList=[];
      $.each(classList, function(index, val) {
        if (val.indexOf(string) === 0) {
          masterList.push(val.substring(string.length));
        }
      });
      $.each(masterList, function(index, val) {
         $('.js-checkbox[data-master='+val+']').prop('checked', false);
      });
    }
  }

  return me;
}());
