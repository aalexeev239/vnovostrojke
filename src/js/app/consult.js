var consult = (function() {
  var me = {},
      $consult = $('#js-consult'),
      $question = $('#js-consult .consult__question'),
      $name = $('#js-consult .consult__name'),
      $submit = $('#js-consult .consult__submit');

  me.init = function() {
    $(document).on('focusin', '#js-consult', onFocusIn);
    $(document).on('focusout', '#js-consult', onFocusOut);
    // $(document).on('keydown', '#js-consult .consult__question', onInputKeydown);
    // $(document).on('keydown', '#js-consult .consult__name', onInputKeydown);
    $(document).on('submit', '#js-consult', onSubmit);
  };

  function onFocusIn() {
    if ($consult.hasClass('closed')) {
      $consult.removeClass('closed');
    }
  }

  function onFocusOut() {
    if (!($question.val() || $name.val())) {
      $consult.addClass('closed');
    }

    // if (!$submit.hasClass('disabled') && (!$question.val() || !$name.val())) {
    //   $submit.addClass('disabled');
    // }
  }

  // function onInputKeydown() {
  //   if ($submit.hasClass('disabled')) {
  //     if ($name.val() && $question.val()) {
  //       console.log('rem');
  //       $submit.removeClass('disabled');
  //     }
  //   }
  // }

  function onSubmit(e) {
    e.preventDefault();
    // if ($submit.hasClass('disabled') || !$question.val() || !$name.val()) {
    if ($submit.hasClass('disabled') || !$question.val() || !$name.val()) {
      return false;
    } else {
      // do smth
    }
  }

  return me;
}());