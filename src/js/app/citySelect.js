var citySelect = (function() {
  var me = {};

  me.init = function() {
    $(document).on('tap', '.js-city-toggle', cityToggle);
    $(document).on('tap', '.js-city-close', cityClose);
    $(document).on('keydown', cityCloseKeyDown);
    $(document).on('tap', checkoutCitySelect);
  };

  function cityToggle(e) {
    e.preventDefault();
    $('#js-city-select').slideToggle(400);
  }

  function cityClose(e) {
    e.preventDefault();
    $('#js-city-select').slideUp(400);
  }

  function cityCloseKeyDown(e) {
    if (e.which == 27) {
      $('#js-city-select:visible').slideUp(400);
    }
  }

  function checkoutCitySelect(e) {
    var $citySelect = $('#js-city-select');
    if ($citySelect.is(':visible')) {
      var $target = $(e.target);
      if (!(
          $target.hasClass('js-city-toggle') ||
          $target.closest('#js-city-select').length ||
          $target.closest('js-city-toggle').length))
      {
        $citySelect.slideUp(400);
      }
    }
  }

  return me;
}());