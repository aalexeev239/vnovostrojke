var asideFixed = (function() {
  var me = {};

  // .js-fixed
  // .js-fixed-wrap
  me.init = function() {
    var $fixed = $('.js-fixed'),
        $wrap  = $('.js-fixed-wrap'),
        maxTimerSteps = 3000;



    // checking if banners loaded every 200ms
    // maximum 3000 attempts (total 600 000 ms = 600sec = 10min)
    var timer = setTimeout(function() {
      timer = setTimeout(arguments.callee, 200)
      console.log('timer' + maxTimerSteps);
      // bind handlers if success

      if (bannersLoaded()) {
        clearTimeout(timer);
        $(window).on('resize',fixedInit);
        fixedInit();
      }

      // or stop
      if (maxTimerSteps-- === 0) {
        clearTimeout(timer);
      }
    }, 200);




    function bannersLoaded() {
      var $banners = $fixed.find('.bwrap'),
          count = $banners.length;
      for (var i = count - 1; i >= 0; i--) {
        var $ban = $($banners[i]).children().eq(1),
            $img = $ban.find('img');

        if ($ban.length) {
          if ($img.length) {
            ($img.outerHeight() > 0) && count--;
          } else {
            count--;
          }
        }
      }
      return (count === 0);
    }





    function fixedInit() {
      var fixH = $fixed.outerHeight(),
          wrapW = $wrap.outerWidth(),
          winH = $(window).height(),
          winW = $(window).width();

      if (fixH < winH && wrapW <= winW) {
        $(window).on('scroll', onWindowScroll);
      } else {
        $(window).off('scroll', onWindowScroll);
        $fixed.removeClass('fixed').removeClass('bottom').css('top',0);
      }
    }




    function onWindowScroll(ev) {
      var sT = $(window).scrollTop(),
          fixH = $fixed.outerHeight(),
          scrollBegin = $wrap.offset().top, // may vary
          maxScroll = $wrap.outerHeight() - fixH, // may vary
          diff = sT - scrollBegin;

      if (diff >= 0) {
        if (diff <= maxScroll) {
          $fixed.addClass('fixed').removeClass('bottom').css('top',0);
        } else {
          $fixed.removeClass('fixed').addClass('bottom').css('top', maxScroll);
        }
      } else {
        $fixed.removeClass('fixed').removeClass('bottom').css('top',0);
      }
    }
  };


  return me;
}());