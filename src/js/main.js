// Андрей Алексеев [AA]
// alexeev.andrey.a@gmail.com


$(document).ready(function() {

  /*===============================
  =            PLUGINS            =
  ===============================*/

  //smoothscroll
  // $('a[href*=#]:not([href=#])').click(function() {
  //    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
  //      var target = $(this.hash);
  //      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
  //      if (target.length) {
  //        $('html,body').animate({
  //          scrollTop: target.offset().top
  //        }, 1500);
  //        return false;
  //      }
  //    }
  //  });

  $('input, textarea').placeholder();
  $('textarea').autosize({
    "placeholder": false,
    "className": 'leave-comment__textarea'
  });

  /*-----  End of PLUGINS  ------*/


  /*==============================
  =            BLOCKS            =
  ==============================*/

  // like btn
  if ($('.js-rating-like').length) {
    $(document).on('click','.rating-like', function (e) {
      e.preventDefault();
      if(! $(this).hasClass('disabled')) {
        $(this).toggleClass('active');
      }
    });
  }

  // text-preview
  if ($('.js-text-preview').length) {
    var closedHeight = 190;

    $.each($('.js-text-preview'), function() {
       var previewHeight = + $(this).removeClass('closed').outerHeight(true);
      // console.log('previewHeight = '+previewHeight);
        if (previewHeight > closedHeight) {
          $(this).css('height', previewHeight+'px').addClass('closed');
        } else {
          $(this).find('.text-preview__control').hide();
        }
    });

    $(document).on('click','.text-preview__control', function (e) {
      e.preventDefault();
      $(this).closest('.js-text-preview').toggleClass('closed');
    });
  }


  // tabs
  $('.js-tabs').length && tabs.init();

  // comment
  $('.js-leave-comment').length && leaveComment.init();

  $('.js-toggle-likes').length && toggleLikes.init();

  $('.js-subscribe').length && subscribeForm.init();

  $('#js-city-select').length && citySelect.init();

  $('.js-search').length && search.init();

  $('.js-dropdown').length && dropdown.init();

  $('.js-fav-btn').length && favorite.init();

  $('.js-show-more').length && showMore.init();

  $('#js-fixed-nav').length && fixedNav.init();

  $('#js-consult').length && consult.init();

  $('.js-gallery').length && gallery.init();

  $('.js-slider').length && slider.init();

  $('.js-article-slider').length && articleSlider.init();

  // $('.js-search-slider').length && searchSlider.init();

  $('.js-modal').length && modals.init();

  $('.js-toggle-content').length && toggleContent.init();

  $('.js-popup').length && popup.init();

  $('.js-checkbox').length && checkboxDependencies.init();

  $('.js-fixed').length && asideFixed.init();

  /*-----  End of BLOCKS  ------*/



});
// END doc.ready