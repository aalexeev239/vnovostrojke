var articleSlider = (function() {
  var me = {};

  me.init = function() {
    $('.js-article-slider').slick({
      slide: "a",
      slidesToShow: 4
    });
  };

  return me;
}());
var asideFixed = (function() {
  var me = {};

  // .js-fixed
  // .js-fixed-wrap
  me.init = function() {
    var $fixed = $('.js-fixed'),
        $wrap  = $('.js-fixed-wrap'),
        maxTimerSteps = 3000;



    // checking if banners loaded every 200ms
    // maximum 3000 attempts (total 600 000 ms = 600sec = 10min)
    var timer = setTimeout(function() {
      timer = setTimeout(arguments.callee, 200)
      console.log('timer' + maxTimerSteps);
      // bind handlers if success

      if (bannersLoaded()) {
        clearTimeout(timer);
        $(window).on('resize',fixedInit);
        fixedInit();
      }

      // or stop
      if (maxTimerSteps-- === 0) {
        clearTimeout(timer);
      }
    }, 200);




    function bannersLoaded() {
      var $banners = $fixed.find('.bwrap'),
          count = $banners.length;
      for (var i = count - 1; i >= 0; i--) {
        var $ban = $($banners[i]).children().eq(1),
            $img = $ban.find('img');

        if ($ban.length) {
          if ($img.length) {
            ($img.outerHeight() > 0) && count--;
          } else {
            count--;
          }
        }
      }
      return (count === 0);
    }





    function fixedInit() {
      var fixH = $fixed.outerHeight(),
          wrapW = $wrap.outerWidth(),
          winH = $(window).height(),
          winW = $(window).width();

      if (fixH < winH && wrapW <= winW) {
        $(window).on('scroll', onWindowScroll);
      } else {
        $(window).off('scroll', onWindowScroll);
        $fixed.removeClass('fixed').removeClass('bottom').css('top',0);
      }
    }




    function onWindowScroll(ev) {
      var sT = $(window).scrollTop(),
          fixH = $fixed.outerHeight(),
          scrollBegin = $wrap.offset().top, // may vary
          maxScroll = $wrap.outerHeight() - fixH, // may vary
          diff = sT - scrollBegin;

      if (diff >= 0) {
        if (diff <= maxScroll) {
          $fixed.addClass('fixed').removeClass('bottom').css('top',0);
        } else {
          $fixed.removeClass('fixed').addClass('bottom').css('top', maxScroll);
        }
      } else {
        $fixed.removeClass('fixed').removeClass('bottom').css('top',0);
      }
    }
  };


  return me;
}());
var checkboxDependencies = (function() {
  var me = {};

  me.init = function() {
    $(document).on('change', '.js-checkbox', onCheckboxMasterChange);
    $(document).on('change', '.js-checkbox-slave', onCheckboxSlaveChange);
  };


  function onCheckboxMasterChange(){
    var $self = $(this),
        isChecked = $self.is(':checked'),
        slaveValue = $self.data('master');
    if (slaveValue) {
      $('.js-checkbox-slave-'+slaveValue).prop('checked',isChecked);
    }
  }

  function onCheckboxSlaveChange(){
    var $self = $(this);
    if (!$self.is(':checked')) {
      var classList = $self.attr('class').split(/\s+/),
          string = 'js-checkbox-slave-',
          masterList=[];
      $.each(classList, function(index, val) {
        if (val.indexOf(string) === 0) {
          masterList.push(val.substring(string.length));
        }
      });
      $.each(masterList, function(index, val) {
         $('.js-checkbox[data-master='+val+']').prop('checked', false);
      });
    }
  }

  return me;
}());

var citySelect = (function() {
  var me = {};

  me.init = function() {
    $(document).on('tap', '.js-city-toggle', cityToggle);
    $(document).on('tap', '.js-city-close', cityClose);
    $(document).on('keydown', cityCloseKeyDown);
    $(document).on('tap', checkoutCitySelect);
  };

  function cityToggle(e) {
    e.preventDefault();
    $('#js-city-select').slideToggle(400);
  }

  function cityClose(e) {
    e.preventDefault();
    $('#js-city-select').slideUp(400);
  }

  function cityCloseKeyDown(e) {
    if (e.which == 27) {
      $('#js-city-select:visible').slideUp(400);
    }
  }

  function checkoutCitySelect(e) {
    var $citySelect = $('#js-city-select');
    if ($citySelect.is(':visible')) {
      var $target = $(e.target);
      if (!(
          $target.hasClass('js-city-toggle') ||
          $target.closest('#js-city-select').length ||
          $target.closest('js-city-toggle').length))
      {
        $citySelect.slideUp(400);
      }
    }
  }

  return me;
}());
var consult = (function() {
  var me = {},
      $consult = $('#js-consult'),
      $question = $('#js-consult .consult__question'),
      $name = $('#js-consult .consult__name'),
      $submit = $('#js-consult .consult__submit');

  me.init = function() {
    $(document).on('focusin', '#js-consult', onFocusIn);
    $(document).on('focusout', '#js-consult', onFocusOut);
    // $(document).on('keydown', '#js-consult .consult__question', onInputKeydown);
    // $(document).on('keydown', '#js-consult .consult__name', onInputKeydown);
    $(document).on('submit', '#js-consult', onSubmit);
  };

  function onFocusIn() {
    if ($consult.hasClass('closed')) {
      $consult.removeClass('closed');
    }
  }

  function onFocusOut() {
    if (!($question.val() || $name.val())) {
      $consult.addClass('closed');
    }

    // if (!$submit.hasClass('disabled') && (!$question.val() || !$name.val())) {
    //   $submit.addClass('disabled');
    // }
  }

  // function onInputKeydown() {
  //   if ($submit.hasClass('disabled')) {
  //     if ($name.val() && $question.val()) {
  //       console.log('rem');
  //       $submit.removeClass('disabled');
  //     }
  //   }
  // }

  function onSubmit(e) {
    e.preventDefault();
    // if ($submit.hasClass('disabled') || !$question.val() || !$name.val()) {
    if ($submit.hasClass('disabled') || !$question.val() || !$name.val()) {
      return false;
    } else {
      // do smth
    }
  }

  return me;
}());
var dropdown = (function() {
  var me = {};

  me.init = function() {
    $(document).on('tap', '.js-dropdown-control', onControlClick);
    $(document).on('keydown', dropdownCloseKeyDown);
    $(document).on('tap', '.js-dropdown-close', onCloseBtnClick);
    // clicking outside
    $(document).on('tap', 'html', closeDropdown);
    $(document).on('tap', '.js-dropdown-body ', preventCloseDropdown);
  };

  function onControlClick(e) {
    e.preventDefault();
    e.stopPropagation();
    $(this).closest('.js-dropdown').toggleClass('active');
  }

  function dropdownCloseKeyDown(e) {
    if (e.which == 27) {
      $('.js-dropdown.active').removeClass('active');
    }
  }

  function onCloseBtnClick(e) {
    e.preventDefault();
    $(this).closest('.js-dropdown').removeClass('active');
  }

  function closeDropdown() {
    $('.js-dropdown.active').removeClass('active');
  }

  function preventCloseDropdown(e) {
    e.stopPropagation();
  }

  return me;
}());
var favorite = (function() {
  var me = {};

  me.init = function() {
    $(document).on('tap', '.js-fav-btn', onFavClick);
  };

  function onFavClick(e) {
    e.preventDefault();
    $(this).toggleClass('active');
  }

  return me;
}());
var fixedNav = (function() {
  var me = {},
      lastscroll = 0,
      $fixedNav = $('#js-fixed-nav'),
      $fixedNavReturn = $('#js-fixed-nav-return'),
      $originalNav = $('#js-fixed-nav-origin');


  me.init = function() {
    $(window).on('scroll', onScroll);
  };

  function onScroll() {
    var currentScroll = $(this).scrollTop(),
        originalNavOffset = $originalNav.length ? $originalNav.offset().top : 0;


    if (currentScroll < originalNavOffset) {
        $fixedNav.slideUp(200);
        if ($fixedNavReturn.length) {
          $fixedNavReturn.addClass('hidden');
        }

    } else {
      $fixedNav.slideDown(200);
      if ($fixedNavReturn.length) {
        if (currentScroll < lastscroll) {
          $fixedNavReturn.removeClass('hidden');
        } else {
          $fixedNavReturn.addClass('hidden');
        }
      }
    }

    lastscroll = currentScroll;
  }

  return me;
}());
var gallery = (function() {
  var me = {};

  me.init = function() {
    // main slider
    $('.js-gallery-slider').slick({
      arrows: false,
      // accessibility: false,
      onAfterChange: onAfterChange,
      onInit: onInit
    });

    // nav slider
    $('.js-gallery-navs').slick({
      arrows: false,
      infinite: false,
      dots: true,
      // draggable: false,
      accessibility: false
    });

    $(document).on('tap', '.js-gallery-item', onNavsItemClick);
    $(document).on('tap', '.js-gallery-left', onLeftArrowClick);
    $(document).on('tap', '.js-gallery-right', onRightArrowClick);
  };

  function onAfterChange(e) {
    var $slider = e.$slider,
        $navs = $slider.closest('.js-gallery').find('.js-gallery-navs'),
        // currentSlide = $slider.slickCurrentSlide(),
        currentSlide = e.currentSlide,
        navItemsPerSlide = $navs.data('items').length ? $navs.data('items') : 8,
        navsCurrentSlide = $navs.slickCurrentSlide(),
        navsCalculatedSlide = Math.floor(currentSlide / navItemsPerSlide);


    $navs.find('.js-gallery-item.active').removeClass('active');
    $navs.find('.js-gallery-item[data-slide='+currentSlide+']').addClass('active');

    $slider.closest('.js-gallery').find('.js-gallery-current-slide').text(currentSlide+1);

    if (navsCalculatedSlide !== navsCurrentSlide) {
      $navs.slickGoTo(navsCalculatedSlide);
    }
  }

  function onInit(e) {
    var $slider = e.$slider;
    // console.log(e);
    $slider.closest('.js-gallery').find('.js-gallery-total').text(e.slideCount);
  }

  function onNavsItemClick(e) {
    e.preventDefault();
    var $self = $(this);
    if (!$self.hasClass('active')) {
      $self.closest('.js-gallery').find('.js-gallery-slider').slickGoTo($self.data("slide"));
    }
  }

  function onLeftArrowClick(e) {
    e.preventDefault();
    $(this).closest('.js-gallery').find('.js-gallery-slider').slickPrev();
  }

  function onRightArrowClick(e) {
    e.preventDefault();
    $(this).closest('.js-gallery').find('.js-gallery-slider').slickNext();
  }


  return me;
}());
var leaveComment = (function() {
  var me = {};

  me.init = function() {
    $(document).on('focusin','.js-leave-comment.closed',onFocusIn);
    $(document).on('submit','.js-leave-comment',onSubmit);
    $(document).on('focusout keypress','.js-validate',onFocusOut);
  };

  function onFocusIn() {
    $(this).removeClass('closed');

  }

  function onSubmit(e) {
    e.preventDefault();
    var $self = $(this);
    // console.log('self');
    // console.log($self);

    if (validate($self)) {
      // do smth
      console.log('submited');
    }
  }

  function validate($form) {
    var $inputs = $form.find('.js-validate');
    $inputs.each(function(index, el) {
      if (! $(el).val()) {
        $(el).addClass('error');
      } else {
        $(el).removeClass('error');
      }
    });

    if ($inputs.filter('.error').length) {
      $form.find('.js-submit').addClass('disabled');
      return false;
    } else {
      $form.find('.js-submit').removeClass('disabled');
      return true;
    }
  }

  function onFocusOut() {
    var $self = $(this),
        $form = $self.closest('.js-leave-comment'),
        $inputs = $form.find('.js-validate');

    if ($self.hasClass('error') && $self.val()) {
      $self.removeClass('error');
    }

    if ($form.find('.js-submit').hasClass('disabled') && (! $inputs.filter('.error').length)) {
      var isValid = true;

      $inputs.each(function(index, el) {
        if (! $(el).val()) {
          isValid = false;
        }
      });

      if (isValid) {
        $form.find('.js-submit').removeClass('disabled');
      }
    }
  }

  return me;
}());
var modals = (function() {
  var me = {};

  me.init = function() {

    $(document).on('confirm', '.js-modal', onModalConfirmed);

  };

  function onModalConfirmed() {
    var $modal = $(this),
        inst = $.remodal.lookup[$modal.data('remodal')];
    inst.close();
  }

  return me;
}());
var popup = (function() {
  var me = {};

  me.init = function() {
    $(document).on('tap', 'html', closePopup);
    $(document).on('tap mousein', '.js-popup', onPopupTapped);
    $(document).on('tap mouseout', '.js-popup', onPopupTapped);
  };

  function onPopupTapped(e) {
    var $self = $(this);
    e.preventDefault();
    e.stopPropagation();
    // $self.toggleClass('hover');
    if (! $self.hasClass('hover')) {
      $('.js-popup.hover').removeClass('hover');
      $self.addClass('hover');
    } else {
      $self.removeClass('hover');
    }
  }

  function closePopup() {
    $('.js-popup.hover').removeClass('hover');
  }

  return me;
}());
// SelectBox https://github.com/marcj/jquery-selectBox

var search = (function() {
  var me = {};


  // js-search
  // js-search-slider
  // js-search-slider-slides
  // js-search-tab
  me.init = function() {

    // Refactoring to new search
    // [AA] 2015/05/10
    /*==========  selects:  ==========*/
    // $('select.search__select')
    //   .selectBox({
    //     mobile: true,
    //     bottomPositionCorrelation: 30
    //   });
    // $('select.search__select').selectBox().change(setShortValue);
    // $('select.search__select').each(setShortValue);
    // checkAdditionalInfo();
    //   changePriceValue();
    //   setPriceToAdvanced();
    /*==========  submit  ==========*/
    //$(document).on('submit', '.js-search', onSearchSubmit);
    // $('.js-search').on('search.success', onSearchSuccess)

    $('.js-search-slider').filter(':visible').each(function(index, el) {
      me.initSlider($(this));
    });

    $(document).on('click', '.js-search-letter', onLetterClicked);

    var $tabs = $('.js-tabs-search');


    // init and reinit sliders on tab change
    $tabs.on('tabs.afterChange', function(event) {

      var $t = $(this);
      var $active = $t.find('.js-tabsItem.active').filter(function(){
        return $(this).closest('.js-tabs').is($t);
      });
      var $sliders = $active.find('.js-search-slider');


      $sliders.each(function(index, el) {
        me.initSlider($(this));
      });
    });


    $(window).on('resize', function() {
      $('.js-search-slider-slides').filter(':not(:visible)').each(function(index, el) {

        if ($(el).hasClass('slick-slider')) $(el).slick('unslick');

        $(el).closest('.js-search-slider').removeData('inited');
      });
    });

  };







  me.initSlider = function($sl, $slides) {

    !$sl.data('inited') && $sl.addClass('loading');

    var imgLoad = imagesLoaded($sl);

    if ($slides) {
      imgLoad.off('done',onSliderImagesLoaded);
      $sl.addClass('loading').data('inited', false);
      $sl.find('.js-search-slider-slides.slick-slider').slick('unslick');
      $sl.html($slides);
    }
    imgLoad.on('done',onSliderImagesLoaded);
  }




  function onSliderImagesLoaded (instance) {
    // cont = js-search-cliser
    var $cont = $(instance.elements[0]),
        $slider = $cont.find('.js-search-slider-slides');

    if (!$cont.data('inited')) {
      $slider.slick({
        slidesToShow: 4,
        adaptiveHeight: true,
        rows: 2
      });
    }

    $cont.data('inited', true).removeClass('loading');
  }



  function onLetterClicked(ev) {
    ev.preventDefault();
    var $letter = $(this),
        $cont = $letter.closest('.js-search-tab');

    $cont.find('.js-search-letter.active').removeClass('active');
    $letter.addClass('active')

    $.ajax({
      // url: encodeURIComponent($letter.attr('href'))+'?nocache='+new Date().getTime()
      url: 'get-by-letter-a.json?'+new Date().getTime(),
      type: 'GET',
      dataType: 'json',
    })
    .done(function(data, response, jqXHR) {
      if (!data) return;

      var length = data.length,
          templ = $('#search-slider-template').html(),
          $slider = $letter.closest('.js-search-tab').find('.js-search-slider'),
          markup;

      Mustache.parse(templ);

      markup = Mustache.render(templ, data);

      $slider.length && me.initSlider($slider, $(markup));
    })
    .fail(function() {
      // console.log("error");
    })
    .always(function() {
      // console.log("complete");
    });
  }



  function addRuleForAddSearch($val){
      $('#advanced').val($val);
      setPriceToAdvanced();
  }



  function onSearchSubmit(e){
    e.preventDefault();
    $(this).trigger('search.success');
  }

  // function onSearchSuccess() {
  //   alert('success');
  //   $(this).find('.js-popover').addClass('popover--shown');
  // }

  // Refactoring to new search
  // [AA] 2015/05/10

  // function checkAdditionalInfo(){
  //     if($('#citySelect').val() == "0"){
  //         $('.search__input-group--location').css('display', 'block');
  //     }else{
  //         $('.search__input-group--location').css('display', 'none');
  //     }
  // }

  // function changePriceValue(){
  //     $('#range-min').change(function() {
  //         getPriceValue();
  //     });

  //     $('#range-max').change(function() {
  //         getPriceValue();
  //     });

  //     $('#price').change(function() {
  //         setPriceToAdvanced();
  //     });
  // }

  // function setPriceToAdvanced()
  // {
  //     var $default = $('#price :selected').val();
  //     if($('#price :selected').val() != 'undefined') {
  //         $priceArray = $('#price :selected').val().split('-');
  //         if($priceArray == 'undefined' || $priceArray == ''){
  //             $priceArray = $default;
  //         }

  //         if ($priceArray[0] != 'undefined' && $priceArray[0] != '') {
  //             $('#range-min').val($priceArray[0]);
  //         } else {
  //             $('#range-min').val('');
  //         }
  //         if ($priceArray[1] != 'undefined' && $priceArray[1] != '') {
  //             $('#range-max').val($priceArray[1]);
  //         } else {
  //             $('#range-max').val('');
  //         }
  //     }
  // }

  // function getPriceValue()
  // {
  //     var $default = $('#price :selected').val();
  //     if($('#range-min').val() != '' && $('#range-min').val() != 'undefined'
  //         && $('#range-max').val() != '' && $('#range-max').val() != 'undefined')
  //     {

  //         $('#price :selected').val($('#range-min').val() + '-' + $('#range-max').val());
  //         $('.search__select-wrap--price .selectBox .selectBox-label')
  //             .text($('#range-min').val() + ' - ' + $('#range-max').val() + ' млн.');
  //     }else if($('#range-max').val() != '' && $('#range-max').val() != 'undefined' &&
  //         ($('#range-min').val() == '' || $('#range-min').val() == 'undefined'))
  //     {

  //         $('#price :selected').val('-' + $('#range-max').val());
  //         $('.search__select-wrap--price .selectBox .selectBox-label')
  //             .text('до ' + $('#range-max').val() + ' млн.');
  //     }else if($('#range-min').val() != '' && $('#range-min').val() != 'undefined' &&
  //         ($('#range-max').val() == '' || $('#range-max').val() == 'undefined'))
  //     {

  //         $('#price :selected').val($('#range-min').val() + '-');
  //         $('.search__select-wrap--price .selectBox .selectBox-label')
  //             .text($('#range-min').val() + '+ млн.' );
  //     }else{
  //         $('#price :selected').val($default);
  //     }
  // }

  // function setShortValue() {
  //   // modified '../plugins/jquery.selectBox.js':675 to get change event after every select
  //   // [AA] 2014/12/09
  //     checkAdditionalInfo();
  //   var $self = $(this),
  //     short = $(this).find('option:selected').first().data('short');
  //   if (short) {
  //     $self.selectBox('control').find('.selectBox-label').text(short);
  //   }
  // }

  // function onInputChanged(){
  //   var $self = $(this),
  //       $tabs = $self.closest('.js-tabs'),
  //       output   = '',
  //       $checked = '';
  //   if ($tabs.length) {
  //     // hide other tabs
  //     $tabs.find('.js-tabsItem:not(.active) input').prop('checked', false);
  //     $checked = $tabs.find('.js-tabsItem.active input:not(.js-checkbox-master):checked');
  //   } else {
  //     $checked = $self.closest('.js-dropdown').find('input:not(.js-checkbox-master):checked');
  //   }
  //   if ($checked.length > 1) {
  //     output = '' + $checked.length + ' ';
  //     output += $checked.first().data("item") ? $checked.first().data("item") : 'объектов';
  //   } else if ($checked.length === 1) {
  //     output = $checked.closest('.checkbox').find('.checkbox__label').text();
  //   }

  //   if (output.length) {
  //     $self.closest('.js-dropdown').find('.js-dropdown-control').html(output);
  //   } else {
  //     $self.closest('.js-dropdown').find('.js-dropdown-control').html('<span class="muted">выберите расположение</span>');
  //   }
  // }

  return me;
}());

var showMore = (function() {
  var me = {};

  me.init = function() {
    $(document).on('tap', '.js-show-more', onMasterClick);
  };

  // Shows element by id written in 'href' by default
  // If data-target is specified and selector it contains doesn't return empty values
  // elements with that selector will be shown
  function onMasterClick(e) {
    var $self = $(this),
        $target = $($self.data('target')).length ? $($self.data('target')) : $($self.attr('href'));

    e.preventDefault();
    if ($target.length) {
      $target.filter(':hidden').slideDown(400);
    }
    $self.hide();
  }

  return me;
}());
var slider = (function() {
  var me = {};

  me.init = function() {
    $('.js-slider').slick({
      arrows: false,
      slide: "img",
      onAfterChange: onAfterChange,
      onInit: onInit
    });

    $(document).on('tap', '.js-slider-left', onLeftArrowClick);
    $(document).on('tap', '.js-slider-right', onRightArrowClick);
  };

  function onAfterChange(e) {
    e.$slider
      .find('.js-slider-current-slide')
      .text(e.currentSlide+1);
  }

  function onInit(e) {
    e.$slider
      .find('.js-slider-total')
      .text(e.slideCount);
  }

  function onLeftArrowClick(e) {
    e.preventDefault();
    $(this)
      .closest('.js-slider')
      .slickPrev();
  }

  function onRightArrowClick(e) {
    e.preventDefault();
    $(this)
      .closest('.js-slider')
      .slickNext();
  }



  return me;
}());
var subscribeForm = (function() {
  var me = {};

  me.init = function() {
    $(document).on('submit','.js-subscribe form',onSubmit);
    $('.js-subscribe').on('success', onSuccess);
  };

  function onSubmit(e) {
    e.preventDefault();
    $(this).closest('.js-subscribe').trigger('success');
  }

  function onSuccess() {
    $(this).addClass('success');
  }

  return me;
}());
var tabs = (function() {
  var me = {};

  // .js-tabs / .js-tabs-search
  // .js-tabsNavItem
  // .js-tabsNavLink
  // .js-tabsItem
  me.init = function() {
    $(document).on('tap','.js-tabsNavLink',onTabClick);
    onPageReady();
  };

  // scan tabs on page loaded. If no active tab-nav found, select first tab-nav and its tab
  function onPageReady() {
    $('.js-tabs').each(function(){
      var $self = $(this);

      // if (! ($self.hasClass('.js-tabs-search') || $self.find('.js-tabsNavItem.active').length)) {
      //   $self.find('.js-tabsNavItem .js-tabsNavLink').first().trigger('click');
      // }
    });

    if ($('.js-tabs-search').length) {
      $(document).on('click', function(e){
        e = e || window.event;
        if (! $(e.target).closest('.js-tabs-search').length) {
          hideSearchTabs();
        }
      });

      $(document).on('keyup', function(e){
        if (e.keyCode === 27) {
          hideSearchTabs();
        };
      });
    }
  }

  function hideSearchTabs() {
    $('.js-tabs-search').find('.js-tabsItem.active').filter(function(){
      return $(this).closest('.js-tabs').hasClass('js-tabs-search');
    }).removeClass('active');
    $('.js-tabs-search').find('.js-tabsNavItem.active').filter(function(){
      return $(this).closest('.js-tabs').hasClass('js-tabs-search');
    }).removeClass('active');
  }


  function onTabClick(e) {
    e = e || window.event;
    var $self = $(e.target),
        $tabsNav = $self.closest('.js-tabsNavItem'),
        $tabs = $self.closest('.js-tabs');

    e.preventDefault();

    if (! $tabsNav.hasClass('active')) {
      // you can provide additional selector via data-toggle attribute,
      // if you want to show/hide more than one element per click
      var activeTab = $self.data('toggle') ? $self.data('toggle') : $self.attr('href');

      if ($(activeTab).length) {

        $tabs.trigger('tabs.beforeChange');

        $tabs.find('.js-tabsItem.active').filter(function(){
          return $(this).closest('.js-tabs').is($tabs);
        }).removeClass('active');
        $tabs.find('.js-tabsNavItem.active').filter(function(){
          return $(this).closest('.js-tabs').is($tabs);
        }).removeClass('active');

        $(activeTab).filter('.js-tabsItem').addClass('active');
        $tabsNav.addClass('active');

        $tabs.trigger('tabs.afterChange');
      }
    } else if ($tabs.hasClass('js-tabs-search')) {
      $tabsNav.removeClass('active');
      $tabs.find('.js-tabsItem.active').filter(function(){
        return $(this).closest('.js-tabs').is($tabs);
      }).removeClass('active');
    }
  }

  me.tabClick = onTabClick;

  return me;
}());
var toggleContent = (function() {
  var me = {};

  me.init = function() {
    $(document).on('tap', '.js-toggle-content', onMasterClick);
  };

  // Shows element by id written in 'href' by default
  // If data-target is specified and selector it contains doesn't return empty values
  // elements with that selector will be shown
  function onMasterClick(e) {
    var $self = $(this),
        $target = $($self.data('target')).length ? $($self.data('target')) : $($self.attr('href'));
    e.preventDefault();
    if ($target.length) {
      $target.slideToggle(400)
    }
    $self.toggleClass('active');
  }

  return me;
}());
var toggleLikes = (function() {
  var me = {};

  me.init = function() {
    $('.js-counter').each(function(){
      updateCounter($(this),'init',0);
    });
    $(document).on('tap','.js-like',onLikeClick);
    $(document).on('tap','.js-dislike',onDislikeClick);
  };

  function onLikeClick(e) {
    e.preventDefault();
    var $self = $(this),
        $counter = $(this).closest('.js-toggle-likes').find('.js-counter');
    if ($self.hasClass('active')) {
      $self.removeClass('active');
      updateCounter($counter,'minus',1);
    } else {
      $self.addClass('active')
      var $dislike = $self.closest('.js-toggle-likes').find('.js-dislike');
      if ($dislike.hasClass('active')) {
        $dislike.removeClass('active');
        updateCounter($counter,'plus',2);
      } else {
        updateCounter($counter,'plus',1);
      }

    }
  }

  function onDislikeClick(e) {
    e.preventDefault();
    var $self = $(this),
        $counter = $(this).closest('.js-toggle-likes').find('.js-counter');
    if ($self.hasClass('active')) {
      $self.removeClass('active');
      updateCounter($counter,'plus',1);
    } else {
      $self.addClass('active');
      var $like = $self.closest('.js-toggle-likes').find('.js-like');
      if ($like.hasClass('active')) {
        $like.removeClass('active');
        updateCounter($counter,'minus',2);
      } else {
        updateCounter($counter,'minus',1);
      }
    }
  }

  function updateCounter($counter, action, value) {
    var currentValue = + $counter.text();
    switch (action) {
      case 'plus':
        currentValue += value;
        break;
      case 'minus':
        currentValue -= value;
        console.log(currentValue);
        break;
      default:
        break;
    }
    if (currentValue > 0) {
      $counter.addClass('positive').removeClass('negative');
    } else if (currentValue < 0) {
      $counter.addClass('negative').removeClass('positive');
    } else {
      $counter.removeClass('positive negative');
    }
    $counter.text(currentValue);
    return false;
  }

  return me;
}());
// Андрей Алексеев [AA]
// alexeev.andrey.a@gmail.com


$(document).ready(function() {

  /*===============================
  =            PLUGINS            =
  ===============================*/

  //smoothscroll
  // $('a[href*=#]:not([href=#])').click(function() {
  //    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
  //      var target = $(this.hash);
  //      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
  //      if (target.length) {
  //        $('html,body').animate({
  //          scrollTop: target.offset().top
  //        }, 1500);
  //        return false;
  //      }
  //    }
  //  });

  $('input, textarea').placeholder();
  $('textarea').autosize({
    "placeholder": false,
    "className": 'leave-comment__textarea'
  });

  /*-----  End of PLUGINS  ------*/


  /*==============================
  =            BLOCKS            =
  ==============================*/

  // like btn
  if ($('.js-rating-like').length) {
    $(document).on('click','.rating-like', function (e) {
      e.preventDefault();
      if(! $(this).hasClass('disabled')) {
        $(this).toggleClass('active');
      }
    });
  }

  // text-preview
  if ($('.js-text-preview').length) {
    var closedHeight = 190;

    $.each($('.js-text-preview'), function() {
       var previewHeight = + $(this).removeClass('closed').outerHeight(true);
      // console.log('previewHeight = '+previewHeight);
        if (previewHeight > closedHeight) {
          $(this).css('height', previewHeight+'px').addClass('closed');
        } else {
          $(this).find('.text-preview__control').hide();
        }
    });

    $(document).on('click','.text-preview__control', function (e) {
      e.preventDefault();
      $(this).closest('.js-text-preview').toggleClass('closed');
    });
  }


  // tabs
  $('.js-tabs').length && tabs.init();

  // comment
  $('.js-leave-comment').length && leaveComment.init();

  $('.js-toggle-likes').length && toggleLikes.init();

  $('.js-subscribe').length && subscribeForm.init();

  $('#js-city-select').length && citySelect.init();

  $('.js-search').length && search.init();

  $('.js-dropdown').length && dropdown.init();

  $('.js-fav-btn').length && favorite.init();

  $('.js-show-more').length && showMore.init();

  $('#js-fixed-nav').length && fixedNav.init();

  $('#js-consult').length && consult.init();

  $('.js-gallery').length && gallery.init();

  $('.js-slider').length && slider.init();

  $('.js-article-slider').length && articleSlider.init();

  // $('.js-search-slider').length && searchSlider.init();

  $('.js-modal').length && modals.init();

  $('.js-toggle-content').length && toggleContent.init();

  $('.js-popup').length && popup.init();

  $('.js-checkbox').length && checkboxDependencies.init();

  $('.js-fixed').length && asideFixed.init();

  /*-----  End of BLOCKS  ------*/



});
// END doc.ready