// популярные команды
// для сокращений проставь алиас grunt --> g
// grunt build (g b) - собрать проект в public/ и загрузить на гугл диск
// grunt watch (g w)
// grunt imageoptim - для png
// grunt imagemin - для jpg
// grunt sprite - собрать иконки в спрайт

module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt, {scope: 'devDependencies'});
  require('time-grunt')(grunt);

  // 1. Вся настройка находится здесь
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    //конфиги папок
    config: {
      src: 'src',
      dist: 'public',
      staticHtml: 'src',
      gdrive: 'public'
      // gdrive: '../../Google Диск/<%= pkg.name %>/'
    },


    // ===========
    // STYLES TASK
    // ===========


    stylus: {
      compile: {
        options: {
          paths: ['<%= config.src %>/styl'],
          urlfunc: 'url64',
          'include css': true,
          compress: false
        },
        files: {
          '<%= config.src %>/css/style.css': ['<%= config.src %>/styl/*.styl'],
          '<%= config.src %>/css/old-ie.css': ['<%= config.src %>/styl/ie/old-ie.styl'],

        }
      }
    },


    cmq: {
      files: {
        src: '<%= config.src %>/css/style.css',
        dest: '<%= config.src %>/css/'
      }
    },


    autoprefixer: {
      single_file: {
        options: {
          browsers: ['last 3 versions', '> 1%', 'ie 8', 'ie 9', 'Opera 12.1']
        },
        src: '<%= config.src %>/css/style.css'
      },
    },


    //css
    cssmin: {
      options: {
        banner: '/* <%= pkg.name %> build <%= grunt.template.today("yyyy-mm-dd") %>; author: <%= pkg.author.name %>; email: <%= pkg.author.email %> */'
      },
      dist: {
        src: '<%= config.src %>/css/style.css',
        dest: '<%= config.src %>/css/style.min.css'
      }
    },

    // END STYLES TASK





    // ===========
    // SCRIPTS TASK
    // ===========

    concat: {
      // Если хочется отдельно плагины и код
      plugins: {
        src: [
          '<%= config.src %>/js/plugins/*.js'
        ],
        dest: '<%= config.src %>/js/build/vendor/plugins.js'
      },
      app: {
        src: [
          '<%= config.src %>/js/app/*.js',
          '<%= config.src %>/js/main.js'
        ],
        dest: '<%= config.src %>/js/build/scripts.js'
      }
      // all: {
      //   src: [
      //     '<%= config.src %>/js/plugins/*.js',
      //     '<%= config.src %>/js/app/*.js',
      //     '<%= config.src %>/js/main.js'
      //   ],
      //   dest: '<%= config.src %>/js/build/scripts.js'
      // }
    },


    //минификация
    uglify: {
      // Если хочется отдельно плагины и код
      plugins: {
        src: '<%= config.src %>/js/build/vendor/plugins.js',
        dest: '<%= config.src %>/js/build/vendor/plugins.min.js'
      },
      js: {
        options: {
          //добавляем дату компиляции
          banner: '/* <%= pkg.name %> build <%= grunt.template.today("yyyy-mm-dd") %>; */'
        },
        src: '<%= config.src %>/js/build/scripts.js',
        dest: '<%= config.src %>/js/build/scripts.min.js'
      }
    },

    // END SCRIPTS TASK




    // ===========
    // HTML TASK
    // ===========


    includereplace: {
      html: {
        src: '*.html',
        dest: '<%= config.staticHtml %>/',
        expand: true,
        cwd: '<%= config.staticHtml %>/_pages/',
        rename: function(dest, src) {
          if (src.indexOf('_') === 0) {
            return dest + src.substring(1);
          }
          else {
            return dest + src;
          }
        }
      }
    },


    prettify: {
      options: {
        config: '.htmlprettifyrc'
      },
      all: {
        expand: true,
        cwd: '<%= config.staticHtml %>',
        ext: '.html',
        src: ['*.html'],
        dest: '<%= config.staticHtml %>/'
      }
    },


    // END HTML TASK





    // ===========
    // SVG TASK
    // ===========

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= config.src %>/!svg-src',
          src: ['!!ai','*.svg'],
          dest: '<%= config.src %>/svgicon/svgs'
        }]
      }
    },

    grunticon: {
      mysvg: {
        files: [{
          expand: true,
          cwd: '<%= config.src %>/svgicon/svgs',
          src: ['*.svg', '*.png'],
          dest: '<%= config.src %>'
        }],
        options: {
          enhanceSVG   : true,
          datasvgcss   : 'css/grunticon-icons.data.svg.css',
          datapngcss   : 'css/grunticon-icons.data.png.css',
          urlpngcss    : 'css/grunticon-icons.fallback.css',
          previewhtml  : '!grunticon-preview.html',
          pngfolder    : 'svgicon/png-grunticon',
          pngpath      : '../svgicon/png-grunticon',
          loadersnippet: 'svgicon/grunticon.loader.js',
          template     : '<%= config.src %>/!svg-src/!grunticon-template.hbs',
          defaultWidth : '100px',
          defaultHeight: '100px',
          colors: {
              white : "#ffffff",
              gray  : "#cccccc",
              green : "#59ab00"
          },
          customselectors: {
            "close-hvr"             :[".icon-close:hover:before"],
            "close-act"             :[
                                      ".icon-close:active:before",
                                      ".icon-close:hover:active:before"
                                      ],
            "dislike-sm-gray"       :[".comment__dislike:before"],
            "dislike-sm-white"      :[".comment__dislike:hover:before"],
            "dislike-sm-red"        :[".comment__dislike:after"],
            "dropdown-control"      :[".selectBox-arrow:before"],
            "dropdown-control-act"  :[
                                      ".icon-dropdown-control:hover:before",
                                      ".selectBox:hover .selectBox-arrow:before",
                                      ".selectBox:active .selectBox-arrow:before",
                                      ".selectBox.active .selectBox-arrow:before"
                                      ],
            "fav-star"              :[".icon-fav-star:before",
                                      ".icon-fav-star.active:active:before",
                                      ".icon-fav-star.active:hover:active:before"],
            "fav-star-hvr"          :[".icon-fav-star:hover:before"],
            "fav-star-act"          :[
                                      ".icon-fav-star:active:before",
                                      ".icon-fav-star:hover:active:before",
                                      ".icon-fav-star.active:before",
                                      ".icon-fav-star.active:hover:before"
                                      ],
            "like-gray"             :[".rating-like:before"],
            "like-green"            :[".rating-like:after"],
            "like-sm-gray"          :[".comment__like:before"],
            "like-sm-white"         :[".comment__like:hover:before"],
            "like-sm-green"         :[".comment__like:after"],
            "star-act"              :[".icon-star.active:before"],
            "star-hvr"              :[".icon-star:hover:before",
                                      ".icon-star:active:before"
                                      ],
            "house-sm-hvr"          :[".icon-house-sm:hover:before"],
            "search-list-sm-hvr"    :[".icon-search-list-sm:hover:before"],
            "wand-sm-hvr"           :[".icon-wand-sm:hover:before"],
            "school-sm-hvr"         :[".icon-school-sm:hover:before"],
            "children-sm-hvr"       :[".icon-children-sm:hover:before"],
            "house-sm-dis"          :[
                                      ".icon-house-sm.disabled:before",
                                      ".icon-house-sm.disabled:hover:before"
                                      ],
            "search-list-sm-dis"    :[
                                      ".icon-search-list-sm.disabled:before",
                                      ".icon-search-list-sm.disabled:hover:before"
                                      ],
            "wand-sm-dis"           :[
                                      ".icon-wand-sm.disabled:before",
                                      ".icon-wand-sm.disabled:hover:before"
                                      ],
            "school-sm-dis"         :[
                                      ".icon-school-sm.disabled:before",
                                      ".icon-school-sm.disabled:hover:before"
                                      ],
            "children-sm-dis"       :[
                                      ".icon-children-sm.disabled:before",
                                      ".icon-children-sm.disabled:hover:before"
                                      ],
            "slick-left"             :[
                                      ".slick-prev:before"
                                      ],
            "slick-right"            :[
                                      ".slick-next:before"
                                      ],
            "slick-left-hvr"        :[
                                      ".icon-slick-left:hover:before",
                                      ".slick-prev:hover:before"
                                      ],
            "slick-right-hvr"       :[
                                      ".icon-slick-right:hover:before",
                                      ".slick-next:hover:before"
                                      ],
            "slick-left-act"        :[
                                      ".icon-slick-left:hover:active:before",
                                      ".slick-prev:hover:active:before"
                                      ],
            "slick-right-act"       :[
                                      ".icon-slick-right:hover:active:before",
                                      ".slick-next:hover:active:before"
                                      ],
            "metro-white"           :[
                                      ".tabs__nav-item.active .tabs__nav-link--metro .metro-header:before",
                                      ],
            "next"                  :[
                                      "ul.ul-next li:before",
                                      ],
            "text-preview-down"     :[
                                      ".icon-arrow-rounded-down:before",
                                      ],
            "text-preview-up"       :[
                                      ".icon-arrow-rounded-up:before",
                                      ],
            "collapse-collapsed"    :[
                                      ".icon-collapse.collapsed:before",
                                      ]
          }
        }
      }
    },

    clean: {
      svg: '<%= config.src %>/svgicon',
      dist: '<%= config.dist %>'
    },

    // END SVG TASK




    // ===========
    // IMG TASK
    // ===========


    //сжатие изображений
    imagemin: {
      stuff: {
        files: [{
          expand: true,
          cwd: '<%= config.src %>/files/',
          src: ['**/*.{jpg,gif}'],
          dest: '<%= config.src %>/files/'
        }]
      },

      images: {
        files: [{
          expand: true,
          cwd: '<%= config.src %>/img/',
          src: ['**/*.{jpg,gif}'],
          dest: '<%= config.src %>/img/'
        }]
      }
    },

    // imageoptim: {
    //   options: {
    //     quitAfter: true
    //   },
    //   allPngs: {
    //     options: {
    //       imageAlpha: true,
    //       jpegMini: false
    //     },
    //     src: ['<%= config.src %>/img/**/*.png', '<%= config.src %>/files/**/*.png']
    //   }
    // },

    // END IMG TASK















    // browserSync: {
    //   dev: {
    //     bsFiles: {
    //       src : [
    //         '<%= config.src %>/css/*.css',
    //         '<%= config.src %>/js/build/*.js'
    //       ]
    //     },
    //     options: {
    //       watchTask: true,
    //       server: {
    //         baseDir: './<%= config.src %>'
    //       }
    //     }
    //   }
    // },





    //отслеживание
    watch: {

      styles: {
        files: ['<%= config.src %>/styl/**/*.styl'],
        tasks: ['styles'],
        options: {
          spawn: false,
          livereload: true
        },
      },

      //скрипты минифицировать и подключать
      scripts: {
          files: ['<%= config.src %>/js/**/*.js'],
          tasks: ['scripts'],
          options: {
            spawn: false,
            livereload: true
          },
      },

      html: {
        files: ['<%= config.staticHtml %>/_pages/*.html', '<%= config.staticHtml %>/_components/*.html'],
        tasks: ['html'],
        options: {
          spawn: false,
          livereload: true
        },
      }
    },



    //копирование в папку public
    copy: {
      js: {
        files: [
          { expand: true,
            cwd: '<%= config.src %>/js/build/',
            src: '**/*.js',
            dest: '<%= config.dist %>/js/build/'
          },
          //lib
          {
            expand: true,
            cwd: '<%= config.src %>/js/lib/',
            src: '*.js',
            dest: '<%= config.dist %>/js/lib/'
          }
        ],
      },
      css: {
        expand: true,
        cwd: '<%= config.src %>/css',
        src: ['*.css'],
        // src: ['*.min.css'],
        dest: '<%= config.dist %>/css'
      },
      svg: {
        expand: true,
        cwd: '<%= config.src %>/svgicon',
        src: '**/*',
        dest: '<%= config.dist %>/svgicon'
      },
      fonts: {
        expand: true,
        cwd: '<%= config.src %>/fonts',
        src: '*.{eot,svg,ttf,woff}',
        dest: '<%= config.dist %>/fonts/'
      },
      stuff: {
        expand: true,
        cwd: '<%= config.src %>',
        //файлы, начинающиеся с !, копии не подлежат
        src: ['!!**/*','!**/!*','*.{html,png,ico,txt,php,json}', 'favicon/**/*','fonts/**/*','img/**/*'],
        dest: '<%= config.dist %>'
      },
      gdrive: {
        expand: true,
        cwd: '<%= config.dist %>',
        src: ['**/*'],
        dest: '<%= config.gdrive %>'
      }
    },





    notify: {
      stylus: {
        options: {
          title: 'Готово!',  // optional
          message: 'STYLUS героически скомпилирован', //required
        }
      },
      scripts: {
        options: {
          title: 'Готово!',  // optional
          message: 'Йо чувак! JS ready!', //required
        }
      },
      html: {
        options: {
          title: 'Готово!',
          message: 'Собран статичный html'
        }
      },
      svg: {
        options: {
          title: 'Готово!',
          message: 'svg спрайт сформирован!'
        }
      },
      img: {
        options: {
          title: 'Готово!',
          message: 'Изображения обработаны!'
        }
      },
      build: {
        options: {
          title: 'Готово!',
          message: 'быстро собрал!'
        }
      },
      make: {
        options: {
          title: 'Готово!',
          message: 'make завершен!'
        }
      }
    }
  });

  grunt.registerTask('w', ['watch']);

  // Tasks
  grunt.registerTask('styles', [
    'stylus',
    'autoprefixer',
    'cmq',
    'cssmin',
    'copy:css',
    'notify:stylus'
  ]);

  grunt.registerTask('scripts', [
    'concat',
    'uglify',
    'copy:js',
    'notify:scripts'
  ]);

  grunt.registerTask('html', [
    'includereplace',
    'prettify',
    'notify:html'
  ]);

  grunt.registerTask('svg', [
    'clean:svg',
    'svgmin',
    'grunticon',
    'copy:svg',
    'notify:svg'
  ]);

  grunt.registerTask('img', [
    'imagemin',
    // 'imageoptim',
    'notify:img'
  ]);

  grunt.registerTask('b', [
    'html',
    'styles',
    'scripts',
    'copy:stuff',
    'copy:gdrive',
    'notify:build'
  ]);

  grunt.registerTask('make', [
    'html',
    'styles',
    'scripts',
    'svg',
    'img',
    'copy:stuff',
    'copy:gdrive',
    'notify:make'
  ]);


  grunt.registerTask('default', [
    'html',
    'styles',
    'scripts',
    'watch'
  ]);


  grunt.registerTask('w', [
    // 'browserSync',
    'watch'
  ]);
};